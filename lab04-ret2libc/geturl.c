#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <curl/curl.h>


void hello(const char *s)
{
	puts(s);
}

static int get_webpage(const char *url)
{
#if 0
	CURLcode res;
	CURL *curl = curl_easy_init();
	if (!curl)
		return -1;

	curl_easy_setopt(curl, CURLOPT_URL, url);
	res = curl_easy_perform(curl);
	curl_easy_cleanup(curl);
#endif
	puts("Got it");
	return 0;
}


static void getURL(void)
{
	char buf[64];

	read(STDIN_FILENO, buf, 128);
	get_webpage(buf);
}

int main(int argc, char **argv)
{
	puts("Enter url $");
	getURL();
	return EXIT_SUCCESS;
}
