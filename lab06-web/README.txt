Lab Environment
===============

VirtualBox compatible VM 'SEEDUbuntu12.04.zip'

Get from: http://www.cis.syr.edu/~wedu/seed/lab_env.html

To login 
User ID: seed
Password: dees



XSS
===

Within the VM, you can access the web application at: http://www.xsslabcollabtive.com 

Actual files are at: /var/www/XSS/Collabtive/

You have an account on this project management web application:
username: bob
password: bob

The application is vulnerable to XSS injection attacks.

Try injecting the following script into a page by supplying it as input to one of the input fields.


<script>alert("test");</script>

Got it?


Now let's try exfiltrating the cookie of whoever happens to view this profile.

You can obtain all the cookies assigned to this document through `document.cookie'.

`document.write()' can be used to add content in the document.

Netcat `nc' can be used as a simple server on localhost.

Got it?


Can you use the exfiltrated session ID to hijack the administrator's connection?

You can restart the browser to lose the existing cookie without logging out.



SQL Injection
=============


Within the VM, you can access the web application at: www.sqllabcollabtive.com

Actual files are at: /var/www/XSS/Collabtive/

You have the same account.

The application is vulnerable to SQL injection attacks.


Let's look at the login process. 

The source code handling user actions can be found at:
/var/www/SQL/Collabtive/include/class.user.php

function login($user, $pass)
...
        $sel1 = mysql_query("SELECT ID,name,locale,lastlogin,gender FROM user WHERE (name = '$user' 	OR email = '$user') AND pass = '$pass'");
...



How about modifying the database?

Let's look at another function in that file.

 function edit()
 ...
 		$upd = mysql_query("UPDATE user SET name='$name',email='$email',tel1='$tel1', tel2='$tel2', 
 			company='$company',zip='$zip',gender='$gender',url='$url',adress='$address1',
 			adress2='$address2',state='$state',country='$country',tags='$tags',locale='$locale',
 			avatar='$avatar',rate='$rate' WHERE ID = $id");
 ...

Could you use this query to modify the db?


Try this yourself: https://www.hacksplaining.com/exercises/sql-injection
