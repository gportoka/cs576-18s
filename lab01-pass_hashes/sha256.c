#include <stdio.h>
#include <openssl/sha.h>
#include <string.h>

void sha256(char *string, char outputBuffer[65])
{
	unsigned char hash[SHA256_DIGEST_LENGTH];
	int i = 0;

	SHA256_CTX sha256;
	SHA256_Init(&sha256);
	SHA256_Update(&sha256, string, strlen(string));
	SHA256_Final(hash, &sha256);

	for (i = 0; i < SHA256_DIGEST_LENGTH; i++) {
		sprintf(outputBuffer + (i * 2), "%02x", hash[i]);
	}
	outputBuffer[64] = 0;
}



int main(int argc, char **argv)
{
	int i;
	char hash[65];

	if (argc < 2) {
		printf("Usage: %s <string>\n", argv[0]);
		return 0;
	}

	sha256(argv[1], hash);
	printf("S: %s\n", argv[1]);
	printf("H: %s\n", hash);

	return 0;
}
